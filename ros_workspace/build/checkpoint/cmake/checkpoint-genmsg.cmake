# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "checkpoint: 1 messages, 0 services")

set(MSG_I_FLAGS "-Icheckpoint:/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(checkpoint_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg" NAME_WE)
add_custom_target(_checkpoint_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "checkpoint" "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(checkpoint
  "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/checkpoint
)

### Generating Services

### Generating Module File
_generate_module_cpp(checkpoint
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/checkpoint
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(checkpoint_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(checkpoint_generate_messages checkpoint_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg" NAME_WE)
add_dependencies(checkpoint_generate_messages_cpp _checkpoint_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(checkpoint_gencpp)
add_dependencies(checkpoint_gencpp checkpoint_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS checkpoint_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(checkpoint
  "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/checkpoint
)

### Generating Services

### Generating Module File
_generate_module_eus(checkpoint
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/checkpoint
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(checkpoint_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(checkpoint_generate_messages checkpoint_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg" NAME_WE)
add_dependencies(checkpoint_generate_messages_eus _checkpoint_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(checkpoint_geneus)
add_dependencies(checkpoint_geneus checkpoint_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS checkpoint_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(checkpoint
  "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/checkpoint
)

### Generating Services

### Generating Module File
_generate_module_lisp(checkpoint
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/checkpoint
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(checkpoint_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(checkpoint_generate_messages checkpoint_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg" NAME_WE)
add_dependencies(checkpoint_generate_messages_lisp _checkpoint_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(checkpoint_genlisp)
add_dependencies(checkpoint_genlisp checkpoint_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS checkpoint_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(checkpoint
  "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/checkpoint
)

### Generating Services

### Generating Module File
_generate_module_nodejs(checkpoint
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/checkpoint
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(checkpoint_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(checkpoint_generate_messages checkpoint_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg" NAME_WE)
add_dependencies(checkpoint_generate_messages_nodejs _checkpoint_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(checkpoint_gennodejs)
add_dependencies(checkpoint_gennodejs checkpoint_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS checkpoint_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(checkpoint
  "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/checkpoint
)

### Generating Services

### Generating Module File
_generate_module_py(checkpoint
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/checkpoint
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(checkpoint_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(checkpoint_generate_messages checkpoint_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg/checkpoint.msg" NAME_WE)
add_dependencies(checkpoint_generate_messages_py _checkpoint_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(checkpoint_genpy)
add_dependencies(checkpoint_genpy checkpoint_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS checkpoint_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/checkpoint)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/checkpoint
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(checkpoint_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/checkpoint)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/checkpoint
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(checkpoint_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/checkpoint)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/checkpoint
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(checkpoint_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/checkpoint)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/checkpoint
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(checkpoint_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/checkpoint)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/checkpoint\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/checkpoint
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(checkpoint_generate_messages_py std_msgs_generate_messages_py)
endif()
