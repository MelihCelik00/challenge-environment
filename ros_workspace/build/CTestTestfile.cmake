# CMake generated Testfile for 
# Source directory: /workspace/lib/environment/challenge-environment/ros_workspace/src
# Build directory: /workspace/lib/environment/challenge-environment/ros_workspace/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("checkpoint")
