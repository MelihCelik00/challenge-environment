// Generated by gencpp from file checkpoint/checkpoint.msg
// DO NOT EDIT!


#ifndef CHECKPOINT_MESSAGE_CHECKPOINT_H
#define CHECKPOINT_MESSAGE_CHECKPOINT_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace checkpoint
{
template <class ContainerAllocator>
struct checkpoint_
{
  typedef checkpoint_<ContainerAllocator> Type;

  checkpoint_()
    : checkpoint_name()
    , type()
    , collision()
    , time(0.0)  {
    }
  checkpoint_(const ContainerAllocator& _alloc)
    : checkpoint_name(_alloc)
    , type(_alloc)
    , collision(_alloc)
    , time(0.0)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _checkpoint_name_type;
  _checkpoint_name_type checkpoint_name;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _type_type;
  _type_type type;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _collision_type;
  _collision_type collision;

   typedef double _time_type;
  _time_type time;





  typedef boost::shared_ptr< ::checkpoint::checkpoint_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::checkpoint::checkpoint_<ContainerAllocator> const> ConstPtr;

}; // struct checkpoint_

typedef ::checkpoint::checkpoint_<std::allocator<void> > checkpoint;

typedef boost::shared_ptr< ::checkpoint::checkpoint > checkpointPtr;
typedef boost::shared_ptr< ::checkpoint::checkpoint const> checkpointConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::checkpoint::checkpoint_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::checkpoint::checkpoint_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace checkpoint

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'checkpoint': ['/workspace/lib/environment/challenge-environment/ros_workspace/src/checkpoint/msg'], 'std_msgs': ['/opt/ros/melodic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::checkpoint::checkpoint_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::checkpoint::checkpoint_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::checkpoint::checkpoint_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::checkpoint::checkpoint_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::checkpoint::checkpoint_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::checkpoint::checkpoint_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::checkpoint::checkpoint_<ContainerAllocator> >
{
  static const char* value()
  {
    return "13578512a70c30d0d55a89abcb864f12";
  }

  static const char* value(const ::checkpoint::checkpoint_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x13578512a70c30d0ULL;
  static const uint64_t static_value2 = 0xd55a89abcb864f12ULL;
};

template<class ContainerAllocator>
struct DataType< ::checkpoint::checkpoint_<ContainerAllocator> >
{
  static const char* value()
  {
    return "checkpoint/checkpoint";
  }

  static const char* value(const ::checkpoint::checkpoint_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::checkpoint::checkpoint_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string checkpoint_name\n"
"string type\n"
"string collision\n"
"float64 time\n"
;
  }

  static const char* value(const ::checkpoint::checkpoint_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::checkpoint::checkpoint_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.checkpoint_name);
      stream.next(m.type);
      stream.next(m.collision);
      stream.next(m.time);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct checkpoint_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::checkpoint::checkpoint_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::checkpoint::checkpoint_<ContainerAllocator>& v)
  {
    s << indent << "checkpoint_name: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.checkpoint_name);
    s << indent << "type: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.type);
    s << indent << "collision: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.collision);
    s << indent << "time: ";
    Printer<double>::stream(s, indent + "  ", v.time);
  }
};

} // namespace message_operations
} // namespace ros

#endif // CHECKPOINT_MESSAGE_CHECKPOINT_H
