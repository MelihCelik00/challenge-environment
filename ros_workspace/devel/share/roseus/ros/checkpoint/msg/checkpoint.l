;; Auto-generated. Do not edit!


(when (boundp 'checkpoint::checkpoint)
  (if (not (find-package "CHECKPOINT"))
    (make-package "CHECKPOINT"))
  (shadow 'checkpoint (find-package "CHECKPOINT")))
(unless (find-package "CHECKPOINT::CHECKPOINT")
  (make-package "CHECKPOINT::CHECKPOINT"))

(in-package "ROS")
;;//! \htmlinclude checkpoint.msg.html


(defclass checkpoint::checkpoint
  :super ros::object
  :slots (_checkpoint_name _type _collision _time ))

(defmethod checkpoint::checkpoint
  (:init
   (&key
    ((:checkpoint_name __checkpoint_name) "")
    ((:type __type) "")
    ((:collision __collision) "")
    ((:time __time) 0.0)
    )
   (send-super :init)
   (setq _checkpoint_name (string __checkpoint_name))
   (setq _type (string __type))
   (setq _collision (string __collision))
   (setq _time (float __time))
   self)
  (:checkpoint_name
   (&optional __checkpoint_name)
   (if __checkpoint_name (setq _checkpoint_name __checkpoint_name)) _checkpoint_name)
  (:type
   (&optional __type)
   (if __type (setq _type __type)) _type)
  (:collision
   (&optional __collision)
   (if __collision (setq _collision __collision)) _collision)
  (:time
   (&optional __time)
   (if __time (setq _time __time)) _time)
  (:serialization-length
   ()
   (+
    ;; string _checkpoint_name
    4 (length _checkpoint_name)
    ;; string _type
    4 (length _type)
    ;; string _collision
    4 (length _collision)
    ;; float64 _time
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _checkpoint_name
       (write-long (length _checkpoint_name) s) (princ _checkpoint_name s)
     ;; string _type
       (write-long (length _type) s) (princ _type s)
     ;; string _collision
       (write-long (length _collision) s) (princ _collision s)
     ;; float64 _time
       (sys::poke _time (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _checkpoint_name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _checkpoint_name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _collision
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _collision (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float64 _time
     (setq _time (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get checkpoint::checkpoint :md5sum-) "13578512a70c30d0d55a89abcb864f12")
(setf (get checkpoint::checkpoint :datatype-) "checkpoint/checkpoint")
(setf (get checkpoint::checkpoint :definition-)
      "string checkpoint_name
string type
string collision
float64 time
")



(provide :checkpoint/checkpoint "13578512a70c30d0d55a89abcb864f12")


