// Auto-generated. Do not edit!

// (in-package checkpoint.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class checkpoint {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.checkpoint_name = null;
      this.type = null;
      this.collision = null;
      this.time = null;
    }
    else {
      if (initObj.hasOwnProperty('checkpoint_name')) {
        this.checkpoint_name = initObj.checkpoint_name
      }
      else {
        this.checkpoint_name = '';
      }
      if (initObj.hasOwnProperty('type')) {
        this.type = initObj.type
      }
      else {
        this.type = '';
      }
      if (initObj.hasOwnProperty('collision')) {
        this.collision = initObj.collision
      }
      else {
        this.collision = '';
      }
      if (initObj.hasOwnProperty('time')) {
        this.time = initObj.time
      }
      else {
        this.time = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type checkpoint
    // Serialize message field [checkpoint_name]
    bufferOffset = _serializer.string(obj.checkpoint_name, buffer, bufferOffset);
    // Serialize message field [type]
    bufferOffset = _serializer.string(obj.type, buffer, bufferOffset);
    // Serialize message field [collision]
    bufferOffset = _serializer.string(obj.collision, buffer, bufferOffset);
    // Serialize message field [time]
    bufferOffset = _serializer.float64(obj.time, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type checkpoint
    let len;
    let data = new checkpoint(null);
    // Deserialize message field [checkpoint_name]
    data.checkpoint_name = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [type]
    data.type = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [collision]
    data.collision = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [time]
    data.time = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.checkpoint_name.length;
    length += object.type.length;
    length += object.collision.length;
    return length + 20;
  }

  static datatype() {
    // Returns string type for a message object
    return 'checkpoint/checkpoint';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '13578512a70c30d0d55a89abcb864f12';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string checkpoint_name
    string type
    string collision
    float64 time
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new checkpoint(null);
    if (msg.checkpoint_name !== undefined) {
      resolved.checkpoint_name = msg.checkpoint_name;
    }
    else {
      resolved.checkpoint_name = ''
    }

    if (msg.type !== undefined) {
      resolved.type = msg.type;
    }
    else {
      resolved.type = ''
    }

    if (msg.collision !== undefined) {
      resolved.collision = msg.collision;
    }
    else {
      resolved.collision = ''
    }

    if (msg.time !== undefined) {
      resolved.time = msg.time;
    }
    else {
      resolved.time = 0.0
    }

    return resolved;
    }
};

module.exports = checkpoint;
