; Auto-generated. Do not edit!


(cl:in-package checkpoint-msg)


;//! \htmlinclude checkpoint.msg.html

(cl:defclass <checkpoint> (roslisp-msg-protocol:ros-message)
  ((checkpoint_name
    :reader checkpoint_name
    :initarg :checkpoint_name
    :type cl:string
    :initform "")
   (type
    :reader type
    :initarg :type
    :type cl:string
    :initform "")
   (collision
    :reader collision
    :initarg :collision
    :type cl:string
    :initform "")
   (time
    :reader time
    :initarg :time
    :type cl:float
    :initform 0.0))
)

(cl:defclass checkpoint (<checkpoint>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <checkpoint>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'checkpoint)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name checkpoint-msg:<checkpoint> is deprecated: use checkpoint-msg:checkpoint instead.")))

(cl:ensure-generic-function 'checkpoint_name-val :lambda-list '(m))
(cl:defmethod checkpoint_name-val ((m <checkpoint>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkpoint-msg:checkpoint_name-val is deprecated.  Use checkpoint-msg:checkpoint_name instead.")
  (checkpoint_name m))

(cl:ensure-generic-function 'type-val :lambda-list '(m))
(cl:defmethod type-val ((m <checkpoint>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkpoint-msg:type-val is deprecated.  Use checkpoint-msg:type instead.")
  (type m))

(cl:ensure-generic-function 'collision-val :lambda-list '(m))
(cl:defmethod collision-val ((m <checkpoint>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkpoint-msg:collision-val is deprecated.  Use checkpoint-msg:collision instead.")
  (collision m))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <checkpoint>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkpoint-msg:time-val is deprecated.  Use checkpoint-msg:time instead.")
  (time m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <checkpoint>) ostream)
  "Serializes a message object of type '<checkpoint>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'checkpoint_name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'checkpoint_name))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'type))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'collision))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'collision))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <checkpoint>) istream)
  "Deserializes a message object of type '<checkpoint>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'checkpoint_name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'checkpoint_name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'type) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'type) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'collision) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'collision) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'time) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<checkpoint>)))
  "Returns string type for a message object of type '<checkpoint>"
  "checkpoint/checkpoint")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'checkpoint)))
  "Returns string type for a message object of type 'checkpoint"
  "checkpoint/checkpoint")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<checkpoint>)))
  "Returns md5sum for a message object of type '<checkpoint>"
  "13578512a70c30d0d55a89abcb864f12")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'checkpoint)))
  "Returns md5sum for a message object of type 'checkpoint"
  "13578512a70c30d0d55a89abcb864f12")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<checkpoint>)))
  "Returns full string definition for message of type '<checkpoint>"
  (cl:format cl:nil "string checkpoint_name~%string type~%string collision~%float64 time~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'checkpoint)))
  "Returns full string definition for message of type 'checkpoint"
  (cl:format cl:nil "string checkpoint_name~%string type~%string collision~%float64 time~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <checkpoint>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'checkpoint_name))
     4 (cl:length (cl:slot-value msg 'type))
     4 (cl:length (cl:slot-value msg 'collision))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <checkpoint>))
  "Converts a ROS message object to a list"
  (cl:list 'checkpoint
    (cl:cons ':checkpoint_name (checkpoint_name msg))
    (cl:cons ':type (type msg))
    (cl:cons ':collision (collision msg))
    (cl:cons ':time (time msg))
))
