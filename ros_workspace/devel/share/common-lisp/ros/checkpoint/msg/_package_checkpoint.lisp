(cl:in-package checkpoint-msg)
(cl:export '(CHECKPOINT_NAME-VAL
          CHECKPOINT_NAME
          TYPE-VAL
          TYPE
          COLLISION-VAL
          COLLISION
          TIME-VAL
          TIME
))