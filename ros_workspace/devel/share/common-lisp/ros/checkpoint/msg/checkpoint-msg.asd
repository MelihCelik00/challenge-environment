
(cl:in-package :asdf)

(defsystem "checkpoint-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "checkpoint" :depends-on ("_package_checkpoint"))
    (:file "_package_checkpoint" :depends-on ("_package"))
  ))